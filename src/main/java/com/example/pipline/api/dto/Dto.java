package com.example.pipline.api.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Dto {

    private String name;
    private String description;
}
