package com.example.pipline.api;

import com.example.pipline.api.dto.Dto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping("/test")
    public Dto create() {
        return Dto.builder()
                .name("new")
                .description("new dto")
                .build();
    }

}
